#ifndef __STM32F0xx_H
#define __STM32F0xx_H

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */
  
/**
  * @brief STM32 Family
  */
#if !defined  (STM32F0)
#define STM32F0
#endif /* STM32F0 */

#if !defined (STM32F030x6) && !defined (STM32F030x8) &&                           \
    !defined (STM32F031x6) && !defined (STM32F038xx) &&                           \
    !defined (STM32F042x6) && !defined (STM32F048xx) && !defined (STM32F070x6) && \
    !defined (STM32F051x8) && !defined (STM32F058xx) &&                           \
    !defined (STM32F071xB) && !defined (STM32F072xB) && !defined (STM32F078xx) && !defined (STM32F070xB) && \
    !defined (STM32F091xC) && !defined (STM32F098xx) && !defined (STM32F030xC)
      #error "No specific STM32F0 defined! Define which STM32F0 is being used in your compiler's preprocessor options!"
#endif

#if defined(STM32F030x6)
  #include "stm32f030x6.h"
#elif defined(STM32F030x8)
  #include "stm32f030x8.h"
#elif defined(STM32F031x6)
  #include "stm32f031x6.h"
#elif defined(STM32F038xx)
  #include "stm32f038xx.h"
#elif defined(STM32F042x6)
  #include "stm32f042x6.h"
#elif defined(STM32F048xx)
  #include "stm32f048xx.h"
#elif defined(STM32F051x8)
  #include "stm32f051x8.h"
#elif defined(STM32F058xx)
  #include "stm32f058xx.h"
#elif defined(STM32F070x6)
  #include "stm32f070x6.h"
#elif defined(STM32F070xB)
  #include "stm32f070xb.h"
#elif defined(STM32F071xB)
  #include "stm32f071xb.h"
#elif defined(STM32F072xB)
  #include "stm32f072xb.h"
#elif defined(STM32F078xx)
  #include "stm32f078xx.h"
#elif defined(STM32F091xC)
  #include "stm32f091xc.h"
#elif defined(STM32F098xx)
  #include "stm32f098xx.h"
#elif defined(STM32F030xC)
  #include "stm32f030xc.h"
#endif

//Defines for Izzy's libraries

#include "stm32_hardware_excluded.h"



typedef enum 
{
  RESET = 0, 
  SET = !RESET
} FlagStatus, ITStatus;

typedef enum 
{
  DISABLE = 0, 
  ENABLE = !DISABLE
} FunctionalState;
#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))

typedef enum 
{
  ERROR = 0, 
  SUCCESS = !ERROR
} ErrorStatus;

#define SET_BIT(REG, BIT)     ((REG) |= (BIT))

#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))

#define READ_BIT(REG, BIT)    ((REG) & (BIT))

#define CLEAR_REG(REG)        ((REG) = (0x0))

#define WRITE_REG(REG, VAL)   ((REG) = (VAL))

#define READ_REG(REG)         ((REG))

#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))


//Peripheral drivers


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __STM32F0xx_H */
