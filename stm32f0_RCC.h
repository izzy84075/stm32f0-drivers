#ifndef __STM32F0_RCC_H__
#define __STM32F0_RCC_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>

#include "stm32f0xx.h"

#include "stm32f0_RCC_Conf.h"

typedef enum {
    //AHB peripherals
    RCC_PERIPHERAL_GPIOA = 0,
    RCC_PERIPHERAL_GPIOB,
    RCC_PERIPHERAL_GPIOC,
    RCC_PERIPHERAL_GPIOF,
    RCC_PERIPHERAL_CRC,
    RCC_PERIPHERAL_DMA1,
    RCC_PERIPHERAL_SRAM,
    RCC_PERIPHERAL_FLITF,
    //APB1 peripherals
    RCC_PERIPHERAL_TIM3,
    RCC_PERIPHERAL_TIM14,
    RCC_PERIPHERAL_WWDG,
    RCC_PERIPHERAL_I2C1,
    RCC_PERIPHERAL_PWR,
    //APB2 peripherals
    RCC_PERIPHERAL_SYSCFG,
    RCC_PERIPHERAL_ADC1,
    RCC_PERIPHERAL_TIM1,
    RCC_PERIPHERAL_SPI1,
    RCC_PERIPHERAL_TIM16,
    RCC_PERIPHERAL_TIM17,
    RCC_PERIPHERAL_USART1,
    RCC_PERIPHERAL_DBGMCU,
} RCC_PERIPHERALS_t;

void STM_RCC_ClkInit(void);

void STM_RCC_Peripheral_Reset(RCC_PERIPHERALS_t whichPeripheral);
void STM_RCC_Peripheral_EnableClock(RCC_PERIPHERALS_t whichPeripheral);
void STM_RCC_Peripheral_DisableClock(RCC_PERIPHERALS_t whichPeripheral);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
