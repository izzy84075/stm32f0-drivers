#ifndef __STM32F0_SPI_H__
#define __STM32F0_SPI_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>

#include "stm32f0_SPI_Conf.h"

#include "stm32f0_GPIO.h"

void STM_SPI_Init(void);
void STM_SPI_DeInit(void);

//TODO: FIXME: The F030 line only has one SPI port, but others could have more. Fix this up to support multiples.
//Note that we only support software chip select.
//The hardware chip select modes on STM32 hardware are odd enough
// that I have no idea how they would ever be used, so I'm not even going
// to bother implementing support for them.
//So make sure to select your device before calling SPI_TXRX, and deselct it afterwards!
void STM_SPI_TXRX(uint8_t TXdata[], uint8_t RXdata[], uint16_t dataCount);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif